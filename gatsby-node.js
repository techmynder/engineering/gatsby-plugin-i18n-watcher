const fs = require("fs")
const path = require("path")

const ignoreQueue = new Set()
let languages = []
let langData

exports.pluginOptionsSchema = function ({ Joi }) {
  return Joi.object({
    i18nConfig: Joi.object({
      languages: Joi.array().items(Joi.string()).min(1),
    })
      .description("tbd")
      .unknown(true),
    i18nFilesDir: Joi.string()
      .required()
      .description(
        "The directory where the JSON files are located. Expects no subdirectories."
      )
      .custom(value => (!fs.existsSync(value) ? new Error("nah") : value)),
  })
}

exports.onCreateDevServer = ({ basePath }, { i18nConfig, i18nFilesDir }) => {
  const chokidar = require("chokidar")
  const pageDataDir = path.join(basePath, `public/page-data/`)
  const pageDataDirGlob = path.join(pageDataDir, "**/page-data.json")
  const langDataDir = path.join(basePath, i18nFilesDir)

  if (!fs.existsSync(pageDataDir)) {
    throw new Error("no page data dir found at " + pageDataDir)
  }

  languages = i18nConfig.languages
  refreshLangData(langDataDir)

  chokidar
    .watch(pageDataDirGlob, { ignoreInitial: true })
    .on("add", filePath => writePageData(filePath, langData))
    .on("change", filePath => writePageData(filePath, langData))

  chokidar
    .watch(
      langData.map(ld => ld.path),
      { ignoreInitial: true }
    )
    .on("change", filePath => {
      writeAllPageData(
        pageDataDir,
        refreshLangData(langDataDir),
        path.basename(filePath).slice(0, -5)
      )
    })
}

function readLangData(langDataDir) {
  const internalLangData = languages.map(lang => {
    const langFilePath = path.join(langDataDir, `${lang}.json`)

    if (!fs.existsSync(langFilePath)) {
      throw new Error("langFile not found: " + langFilePath)
    }

    return {
      lang,
      path: langFilePath,
      content: JSON.parse(fs.readFileSync(langFilePath)),
    }
  })

  if (internalLangData.length === 0) {
    throw new Error("no lang data")
  }

  return internalLangData
}

function refreshLangData(langDataDir) {
  return (langData = readLangData(langDataDir))
}

function jsonPath(a) {
  const map = new Map()

  const pathify = (obj, r = "") => {
    if (typeof obj != "object") {
      return true
    }

    for (const key in obj) {
      if (pathify(obj[key], r + "." + key)) {
        map.set((r.substring(1) + "." + key).replace(/^\./, ""), obj[key])
      }
    }

    return false
  }

  pathify(a)

  return map
}

function writeAllPageData(pageDataDir, langData, onlyLang) {
  const pageDataPaths = walkDir(pageDataDir)

  for (const pdPath of pageDataPaths) {
    writePageData(pdPath, langData, onlyLang)
  }
}

function writePageData(pageDataPath, langData, onlyLang) {
  const fileData = JSON.parse(fs.readFileSync(pageDataPath))
  const intl = fileData?.result?.pageContext?.intl

  if (!intl || !intl.language || !intl.messages) {
    console.info("ignoring, no i18n data", pageDataPath)
    return
  }

  if (onlyLang && intl.language !== onlyLang) {
    console.info(
      "ignoring, lang mismatch",
      pageDataPath,
      intl.language,
      onlyLang
    )
    return
  }

  let matchedLang = null
  for (const lang of langData) {
    if (lang.lang === intl.language) {
      matchedLang = lang
      break
    }
  }

  if (!matchedLang) {
    console.error("unexpected lang: " + intl.language)
    return
  }

  if (ignoreQueue.has(pageDataPath)) {
    console.info("i18n :: delete from queue", pageDataPath)
    ignoreQueue.delete(pageDataPath)
    return
  }

  intl.messages = Object.fromEntries(jsonPath(matchedLang.content))
  console.info("i18n :: update page-data", pageDataPath, matchedLang.lang)

  ignoreQueue.add(pageDataPath)

  fs.writeFileSync(pageDataPath, JSON.stringify(fileData, null, 4))
}

function walkDir(dirPath) {
  return fs
    .readdirSync(dirPath, { withFileTypes: true })
    .map(entry => {
      const childPath = path.join(dirPath, entry.name)

      return entry.isDirectory() ? walkDir(childPath) : childPath
    })
    .flat()
    .filter(f => f.endsWith("page-data.json"))
}
